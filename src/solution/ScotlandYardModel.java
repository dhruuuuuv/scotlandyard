package solution;

import scotlandyard.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

public class ScotlandYardModel extends ScotlandYard {
//declares fields to store values passed in
	int numberOfDetectives;
	List<Boolean> rounds;
	Graph<Integer, Route> graph = new Graph<Integer, Route>();
	
	int players = 0;
	int noOfRounds = 0;
	HashMap<Colour, PlayerInfo> playerMap = new HashMap<Colour, PlayerInfo>();
	List<Spectator> spectators = new ArrayList<Spectator>();
	
	boolean isInitialised = false;
	private BlockingQueue<Move> queue;

//list to store the locations that Mr X has visited.
	int mrXLocation;
	
	private Colour currentPlayer;
	
//constructor for the class
    public ScotlandYardModel(int numberOfDetectives, List<Boolean> rounds, String graphFileName) throws IOException {
        super(numberOfDetectives, rounds, graphFileName);
        this.numberOfDetectives = numberOfDetectives;
        this.rounds = rounds;
 
//reads in graph and assigns it to graph field
        ScotlandYardGraphReader greader = new ScotlandYardGraphReader();
        this.graph = greader.readGraph(graphFileName);

//sets Mr X as the currentPlayer, and initializes his location to 0 as he hasn't yet been revealed
        currentPlayer = Colour.Black;
        mrXLocation = 0;
    }
    
//passes a move and notifies all the spectators of that move:
    public void notifySpectators(Move move) {
    	for (Spectator spectator : spectators) {
    		spectator.notify(move);
    	}
    }
    
    private void setBlockingQueue(BlockingQueue<Move> queue) {
    	this.queue = queue;
    }
    
    private BlockingQueue<Move> getBlockingQueue() {
    	return queue;
    }

//notifies the player using the observer pattern of a move they want to make    
    @Override
    protected Move getPlayerMove(Colour colour) {
        PlayerInfo playerInfo = playerMap.get(colour);
        Player player = playerInfo.getPlayer();
        Move move = player.notify(playerInfo.getLocation(), validMoves(colour));
        return move;
    }

//fetches the next player in the list    
    @Override
    protected void nextPlayer() {
    	int index = getPlayers().indexOf(currentPlayer);
    	index++;
    	if (index >= getPlayers().size()) index = 0; 
    	currentPlayer = getPlayers().get(index);
    }

//function to check if Mr X is hidden
  	public boolean isMrXRevealed() {
  		if (getRounds().get(noOfRounds))
  			return true;
  		else
  			return false;
  	}
    
//function to play a move and adjust the location of the player
	@Override
    protected void play(MoveTicket move) {
		if (move.colour == Colour.Black)
			{noOfRounds++;}
		
    	PlayerInfo player = playerMap.get(move.colour);
    	int location = move.target;
    	Ticket ticket = move.ticket;
    	player.setLocation(location);
    	
    	Map<Ticket, Integer> tickets = player.getTickets();
    	int noTickets = tickets.get(ticket);
    	tickets.put(ticket, noTickets-1);
    	player.setTickets(tickets);
    	
    	if (move.colour == Colour.Black && !isMrXRevealed()) {
    		notifySpectators(new MoveTicket(Colour.Black, mrXLocation, move.ticket));
    	}
    	
    	else if (move.colour == Colour.Black && isMrXRevealed()) {
    		notifySpectators(new MoveTicket(Colour.Black, location, move.ticket));
    	}
    	
    	else if (move.colour != Colour.Black) {
    		PlayerInfo mrX = playerMap.get(Colour.Black);
    		Map<Ticket, Integer> mrXTickets = mrX.getTickets();
    		int noMrXTickets = mrXTickets.get(ticket);
    		mrXTickets.put(ticket, noMrXTickets+1);
    		mrX.setTickets(mrXTickets);
    		
        	notifySpectators(move);
    	}
    }

//calls the play method twice with each move, and decrements the number of Double Move Tickets, and notifies the spectators
    @Override
    protected void play(MoveDouble move) {
    	PlayerInfo player = playerMap.get(move.colour);
    	MoveTicket move1 = (MoveTicket) move.moves.get(0);
    	MoveTicket move2 = (MoveTicket) move.moves.get(1);
    	
    	Map<Ticket, Integer> tickets = player.getTickets();
    	int doubleTick = tickets.get(Ticket.DoubleMove);
    	tickets.put(Ticket.DoubleMove, doubleTick-1);
    	player.setTickets(tickets);
    	
    	notifySpectators(move);
    	play(move1);
    	play(move2);
    }

//plays a pass move and notifies the spectators
    @Override
    protected void play(MovePass move) {
    	notifySpectators(move);
    }
    
//produces a list of valid moves that a player of a colour can take
    @Override
    protected List<Move> validMoves(Colour player) {
    	List<Move> moves = new ArrayList<Move>();
    	PlayerInfo activePlayer = getPlayer(player);
    	int location = activePlayer.getLocation();
    	List<Edge<Integer, Route>> connectedEdges = graph.getEdges(location);
    	
    	for (Edge<Integer, Route> edge : connectedEdges) {
    		Route route = edge.data();
    		Ticket ticket = Ticket.fromRoute(route);
    		if (getPlayerTickets(activePlayer.getColour(),ticket) > 0) {
    			int otherEnd = (getDestination(location, edge));
    			if (isValidMove(otherEnd)) {
    				MoveTicket move = new MoveTicket(activePlayer.getColour(),otherEnd,ticket);
    				moves.add(move);
    			}
    		}
    	}
    	int secretMoveTickets = getPlayerTickets(Colour.Black,Ticket.SecretMove);
    	int doubleMoveTickets = getPlayerTickets(Colour.Black,Ticket.DoubleMove);

    	if (player == Colour.Black && secretMoveTickets > 0) {
    		List<Move> allMoves = new ArrayList<Move> (moves);
    		allMoves.addAll(makeSecMoves(moves));
    		moves = allMoves;
    	}
    	
    	if (player == Colour.Black && doubleMoveTickets > 0) {
    		List<Move> allMoves = new ArrayList<Move> (moves);
    		allMoves.addAll(makeDoubMoves(moves));
    		moves = allMoves;
    	}
    	
    	if (moves.isEmpty() && (player != Colour.Black))
    		moves.add(new MovePass(player));
    		
    		
        return moves;
    }
    
//function to produce valid moves with double moves
    protected List<Move> validDoubleMoves(Ticket prevTicket, MoveTicket move) {
    	int prevMoveTickets = getPlayerTickets(Colour.Black,prevTicket);
    	List<Move> moves = new ArrayList<Move>();
    	int location = move.target;
    	List<Edge<Integer, Route>> connectedEdges = graph.getEdges(location);
    	
    	for (Edge<Integer, Route> edge : connectedEdges) {
    		Route route = edge.data();
    		Ticket ticket = Ticket.fromRoute(route);
    		
    		if (prevMoveTickets-1 > 0) {
    			int otherEnd = (getDestination(location, edge));
    			if (isValidMove(otherEnd)) {
    				MoveTicket moveD = new MoveTicket(Colour.Black,otherEnd,ticket);
    				moves.add(moveD);
    			}
    		}
    	}
    	
    	int secretMoveTickets = getPlayerTickets(Colour.Black,Ticket.SecretMove);
    	if (prevTicket == Ticket.SecretMove)
    		secretMoveTickets = secretMoveTickets - 1;
    	
    	if (secretMoveTickets > 0) {
    		List<Move> allMoves = new ArrayList<Move> (moves);
    		allMoves.addAll(makeSecMoves(moves));
    		moves = allMoves;
    		
    	}
        return moves;
    }
   
//produces secret moves if Mr X is the player
    public List<Move> makeSecMoves(List<Move> moves) {
		List<Move> secretMoves = new ArrayList<Move>();
		for (Move move : moves) {
			MoveTicket moveTicket = (MoveTicket) move;
			Colour orgColour = moveTicket.colour;
			int orgTarget = moveTicket.target;
			MoveTicket secMove = new MoveTicket(orgColour,orgTarget,Ticket.SecretMove);
			secretMoves.add(secMove);
		}
		return secretMoves;
    }
    
//produces double moves if Mr X is the player
    public List<Move> makeDoubMoves(List<Move> moves) {
		List<Move> doubleMoves = new ArrayList<Move>();
		for (Move move: moves) {
			MoveTicket moveTicket = (MoveTicket) move;
			Ticket lastMoveTicket = moveTicket.ticket;
			List<Move> secondMoves = validDoubleMoves(lastMoveTicket,moveTicket);
			for (Move secondMove : secondMoves) {
				MoveDouble moveDouble = new MoveDouble(Colour.Black, move, secondMove);
				doubleMoves.add(moveDouble);
			}
		}
		return doubleMoves;
    }
    
 
//function to check if location is a target or source on an edge     
    public int getDestination(int location, Edge<Integer, Route> edge) {
    	int target = edge.target();
 		int source = edge.source();
 		if (location == source) return target;
 		else if (location == target) return source;
 		return -1; 	
    }
    
    
//function to check if a player can move onto a location
    public boolean isValidMove (int location) {
    	for (Colour colour : getPlayers()) {
    		PlayerInfo player = playerMap.get(colour);
    		int loc = player.getLocation();
    		if (location == loc && colour != Colour.Black) {
    			return false;
    		}
    	}
    	return true;
    }

//function to check if Mr X can move onto a location
    public boolean isMrXStuck() {
    	PlayerInfo mrX = playerMap.get(Colour.Black);
    	int location = mrX.getLocation();
		
		List<Integer> locationList = new ArrayList<Integer>();
		
		List<Edge<Integer, Route>> connectedEdges = graph.getEdges(location);
		
		List<Colour> players = getPlayers();
		players.remove(Colour.Black);

		for (Edge<Integer, Route> edge : connectedEdges) {
			int otherEnd = (getDestination(location, edge));
			
			boolean free = true;
			
			for (Colour colour : players) {
				
				PlayerInfo player = playerMap.get(colour);
				int loc = player.getLocation();
				
				if (otherEnd == loc) {
					free = false;
				}
			}
			
			if(free)
				locationList.add(otherEnd);

		}
		
		if (locationList.isEmpty())
			return true;
		else
			return false;
    }
    
//adds a spectator
    @Override
    public void spectate(Spectator spectator) {
    	spectators.add(spectator);
    }

//called when the ScotlandYardModel constructor is called for each player
    @Override
    public boolean join(Player player, Colour colour, int location, Map<Ticket, Integer> tickets) {
        isInitialised = true;
    	PlayerInfo activePlayer = new PlayerInfo(player, colour, location, tickets);
    	playerMap.put(colour, activePlayer);
    	players++;
        return true;
    }

//gets the list of current players in the game
    @Override
    public List<Colour> getPlayers() {
        Set<Colour> playerSet = playerMap.keySet();
        List<Colour> players = new ArrayList<Colour>();
        players.addAll(playerSet);
        return players;
    }

//function to return the PlayerInfo given a colour 
    public PlayerInfo getPlayer(Colour colour) {
    	PlayerInfo player = playerMap.get(colour);
    	return player;
    }

//returns the winning players of a game, either Mr X or the Detectives    
    @Override
    public Set<Colour> getWinningPlayers() {
    	
    	Set<Colour> winners = new HashSet<Colour>();
    	
    	PlayerInfo mrX = playerMap.get(Colour.Black);
    	mrXLocation = mrX.getLocation();
    	
    	if (isMrXStuck()) {
    		return getDetectives();
    	}
    	
    	Set<Colour> setPlayers = getDetectives();
		List<Colour> players = new ArrayList<Colour>();
		players.addAll(setPlayers);
				
		int mrXCurrentLocation = mrX.getLocation();
		
			for (Colour colour : players) {
				
				PlayerInfo player = playerMap.get(colour);
				int location = player.getLocation();
				
				
				if (location == mrXCurrentLocation) {
					return getDetectives();
				}
				
			}
			
    	if (noDetectiveTickets()) {
    		winners.add(Colour.Black);
    		return winners;
    	}
    	
		if (noOfRounds + 1 == getRounds().size()) {
    		winners.add(Colour.Black);
			return winners;
		}
    	
    	if (isGameOver() && winners.isEmpty()) {
    		winners.add(Colour.Black);
    	}
    	
    	
    	return winners;
    }
    
//returns the location of a player given the colour, if Mr X then 0 if not shown, location if his round to show and last location otherwise
    @Override
    public int getPlayerLocation(Colour colour) {
    	int firstLocationX = 100000;
    	
    	for (boolean round : rounds) {
    		if (round == true)
    			firstLocationX = rounds.indexOf(round);
    	}
    	
    	if (colour == Colour.Black && noOfRounds < firstLocationX) {
    		return mrXLocation;
    	}
    	
    	else if (colour == Colour.Black && isMrXRevealed()) {
    		PlayerInfo player = playerMap.get(colour);
    		int location = player.getLocation();
    		mrXLocation = location;
    		return location;
    	}
    	
    	else if (colour == Colour.Black && !isMrXRevealed()) {
    		return mrXLocation;
    	}
    	
		PlayerInfo player = playerMap.get(colour);
		return player.getLocation();
    }

//returns the number of tickets a player/colour has    
    @Override
    public int getPlayerTickets(Colour colour, Ticket ticket) {
        PlayerInfo player = playerMap.get(colour);
        Map<Ticket, Integer> ticketMap = player.getTickets();
        return ticketMap.get(ticket);
    }
    
//returns a boolean as to whether all the detectives are out of tickets
    public boolean noDetectiveTickets() {
    	List<Colour> detectives = getPlayers();
    	detectives.remove(Colour.Black);
    	    	
    	int finishedCounter = 0;
    	
    	for (Colour colour : detectives) {
        	int busTickets = getPlayerTickets(colour, Ticket.Bus);
        	int taxiTickets = getPlayerTickets(colour, Ticket.Taxi);
        	int undergroundTickets = getPlayerTickets(colour, Ticket.Underground);
        	if (busTickets == 0 && taxiTickets == 0 && undergroundTickets == 0) {
        		finishedCounter++;
        	}
    	}
        	if (finishedCounter == numberOfDetectives)
        		return true;

    	return false;
    }
    
//function to return just the detectives playing the game
    public Set<Colour> getDetectives() {
    	Set<Colour> detectives = new HashSet<Colour>();
    	for (Colour player : playerMap.keySet()) {
    		if (!player.equals(Colour.Black))
    			detectives.add(player);
    	}
    	return detectives;
    }

//returns a boolean as to whether the game is over    
    @Override
    public boolean isGameOver() {

//returns false if the game hasn't been set up yet
    	if (!isReady()) {
    		return false;
    	}
//returns true if the number of detectives is 0
    	else if (numberOfDetectives == 0) {
			return true;
    	}
//returns true if Mr X has no valid moves left he can make
    	if (isMrXStuck()) {
			return true;
		}
		
//returns true if the number of rounds is up and Mr X hasn't been caught
		
		Set<Colour> setPlayers = getDetectives();
		List<Colour> players = new ArrayList<Colour>();
		players.addAll(setPlayers);
		
		PlayerInfo mrX = playerMap.get(Colour.Black);
		
		int mrXCurrentLocation = mrX.getLocation();
		
			for (Colour colour : players) {
				
				PlayerInfo player = playerMap.get(colour);
				int location = player.getLocation();
				
				
				if (location == mrXCurrentLocation) {
					return true;
				}
				
				if (validMoves(colour).get(0) instanceof MovePass) {
					return true;
				}
				
			}
			
			if (noOfRounds + 1 == getRounds().size() && getCurrentPlayer() == Colour.Black) {
				return true;
			}
			
			if (noDetectiveTickets()) {
				return true;
			}
			
		return false;
    }
//

    public Map<Colour, PlayerInfo> getPlayerMap() {
    	return playerMap;
    }
    
//returns a boolean if all the players ready to play have joined    
    @Override
    public boolean isReady() {
    	if (playerMap.size()-1 == numberOfDetectives) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

//returns the currentPlayer field from the game object    
    @Override
    public Colour getCurrentPlayer() {
        return currentPlayer;
    }

//returns the current round    
    @Override
    public int getRound() {
        return noOfRounds;
    }

//returns a list of booleans for the rounds
    @Override
    public List<Boolean> getRounds() {
        return rounds;
    }
}
