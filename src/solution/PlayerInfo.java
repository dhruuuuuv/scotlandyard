package solution;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import scotlandyard.*;

public class PlayerInfo implements Player {
	private Player player;
	private Colour colour;
	private int location;
	private Map<Ticket, Integer> tickets;
	
	public PlayerInfo (Player player, Colour colour, int location, Map<Ticket, Integer> tickets) {
		this.player = player;
		this.colour = colour;
		this.location = location;
		this.tickets = tickets;
	}


	void setPlayer(Player player) {
		this.player = player;
	}
	
	Player getPlayer() {
		return player;
	}

	void setColour(Colour colour) {
		this.colour = colour;
	}
	
	Colour getColour() {
		return colour;
	}

	void setLocation(int location) {
		this.location = location;
	}
	
	public int getLocation() {
		return location;
	}
	

	void setTickets(Map<Ticket, Integer> tickets) {
		this.tickets = tickets;
	}
	
	public Map<Ticket, Integer> getTickets() {
		return tickets;
	}
	
	int noOfTickets() {
		int number = 0;
		Collection<Integer> noTickets = tickets.values();
		for (int no : noTickets) {
			number += no;
		}
		return number;
	}


	@Override
	public Move notify(int location, List<Move> list) {
		return list.get(1);
	}
}
