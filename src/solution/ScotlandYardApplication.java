package solution;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.SwingUtilities;

public class ScotlandYardApplication {
	private ScotlandYardModel model;
	private BlockingQueue<Move> queue;

//main function
	public static void main(String[] args) throws IOException {
		ScotlandYardApplication program = new ScotlandYardApplication();
		program.run();
	}

//returns a list of rounds which can be used to construct the scotlandyardmodel
	public static List<Boolean> getRounds() {
        List<Boolean> rounds = new ArrayList<Boolean>();
        rounds.add(false);
        rounds.add(false);
        rounds.add(true);
        rounds.add(false);
        rounds.add(false);
        return rounds;
	}

//called by the main method, sets up game and blocking queue, creates the presenter
	void run() throws IOException {
		queue = new LinkedBlockingQueue<Move>();
		model = new ScotlandYardModel(2, getRounds(), "resources/graph.txt");
		setUpGame(model);
		ScotlandYardPresenter presenter = new ScotlandYardPresenter(model);
	}
		
//function(s) to construct a game and move it around
	public static void setUpGame(ScotlandYardModel model) {
		model.join(new SingleMovePlayer(), Colour.Black, 1, getTickets(true));
		model.join(new SingleMovePlayer(), Colour.Blue, 8, getTickets(false));
		model.join(new SingleMovePlayer(), Colour.Green, 20, getTickets(false));
	}

//to be removed later	
//set up a single player	
	public static class SingleMovePlayer implements  Player {
		public MoveTicket chosenMove;
		public Move notify(int location, List<Move> moves) {
			for(Move move : moves) {
				if(move instanceof MoveTicket) {
					chosenMove = (MoveTicket) move;
					return move;
				}
			}
			return null;
		}
	}

	public final static Colour[] colours = { Colour.Black, Colour.Blue, Colour.Green,
		Colour.Red, Colour.White, Colour.Yellow
	};

	public final static int[] locations = {
		5, 15, 63, 67, 98, 121, 140
	};


	public final static int[] mrXTicketNumbers = { 4, 3, 3, 2, 5 };
	public final static int[] detectiveTicketNumbers = { 11, 8, 4, 0, 0 };
	
    public final static Ticket[] tickets = { Ticket.Taxi, Ticket.Bus,
        Ticket.Underground, Ticket.DoubleMove,
        Ticket.SecretMove };
    
    public static HashMap<Ticket, Integer> getTickets(boolean mrX)
    {
        HashMap<Ticket, Integer> tickets = new HashMap<Ticket, Integer>();
        for (int i = 0; i < ScotlandYardApplication.tickets.length; i++) {
            if(mrX)
                tickets.put(ScotlandYardApplication.tickets[i], ScotlandYardApplication.mrXTicketNumbers[i]);
            else
                tickets.put(ScotlandYardApplication.tickets[i], ScotlandYardApplication.detectiveTicketNumbers[i]);
        }
        return tickets;
    }
	
}
