package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


//plays the role of presenter in the Scotland Yard Ensemble
public class ScotlandYardPresenter implements Spectator, Runnable{
	private int task, START = 0, UPDATE = 1;
	
	private ScotlandYardModel model;
	private Display screen;
	private BlockingQueue<Move> queue;
	
	HashMap<String, List<Integer>> coordinateMap = new HashMap<String, List<Integer>>();
	
//presenter in the MVP model	
	public ScotlandYardPresenter(ScotlandYardModel model) {
		this.model = model;
		model.spectate(this);
		
		task = START;
		
		SwingUtilities.invokeLater(this);
	}

//run on a different thread to the graphics
	public void run() {
		if (task == START)
			start();
		else if (task == UPDATE)
			update();
	}
	
//temporary print function
	void w (Object o) {
		System.out.println(o.toString());
	}

//create the frame to hold everything, add the screen
	void start() {
		JFrame w =  new JFrame();
		w.setTitle("Scotland Yard");
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        prepareGame();
        this.screen = new Display(model.getPlayers(), producePlayerCoordMap(), produceGameStatsMap(), produceCtm());
     
        screen.start();

        w.add(screen);
        w.pack();
        w.setLocationByPlatform(true);
        w.setVisible(true);
	}
	
	HashMap<Colour, HashMap<Ticket, Integer>> produceCtm() {
		HashMap<Colour, HashMap<Ticket, Integer>> ctm = new HashMap<Colour, HashMap<Ticket, Integer>>();
		Set<Colour> dets = model.getDetectives();
		for (Colour colour : dets) {
			PlayerInfo player = model.getPlayer(colour);
			HashMap<Ticket, Integer> playerTickets = new HashMap<Ticket, Integer>();
			playerTickets = (HashMap<Ticket, Integer>) (player.getTickets());
			ctm.put(colour, playerTickets);
		}
		return ctm;
	}

	HashMap<String, String> produceGameStatsMap() {
		HashMap<String, String> gameStatsMap = new HashMap<String, String>();
		
		String roundNumber = Integer.toString(model.getRound());
		String whoseTurn;
		String gameStatus;
		Colour colour = model.getCurrentPlayer();

		switch (colour) {
		case Black: whoseTurn =  "Mr. X";
		break;
		case Blue: whoseTurn =  "Blue Detective";
		break;
		case Green: whoseTurn =  "Green Detective";
		break;
		case White: whoseTurn =  "White Detective";
		break;
		case Red: whoseTurn =  "Red Detective";
		break;
		case Yellow: whoseTurn =  "Yellow Detective";
		break;
		default: whoseTurn =  "whoseTurn error";
		break;
		}
		
		if(model.isGameOver())
			gameStatus = "Game Over!";
		else
			gameStatus = "Game Not Over!";
		
		gameStatsMap.put("gs", gameStatus);
		gameStatsMap.put("wt", whoseTurn);
		gameStatsMap.put("wr", roundNumber);
		
		return gameStatsMap;
	}
	
	HashMap<Colour, List<Integer>> producePlayerCoordMap() {
		HashMap<Colour, List<Integer>> playerCoordMap = new HashMap<Colour, List<Integer>>();
		for (Colour colour : model.getPlayers()) {
			int location = model.getPlayerLocation(colour);
			
			if (location == 0) {
				List<Integer> coords = new ArrayList<Integer>();
				coords.add(-10);
				coords.add(-10);
				playerCoordMap.put(colour, coords);
			}
			
			else {
				List<Integer> coords = coordinateMap.get(Integer.toString(location));
				playerCoordMap.put(colour, coords);
			}
		}
		return playerCoordMap;
	}
	
	void update() {
		screen.update(producePlayerCoordMap(), produceGameStatsMap(), produceCtm());
	}
	
	void prepareGame() {
		ScotlandYardGraphReader reader = new ScotlandYardGraphReader();
		try {
		Graph<Integer, Route> graph = reader.readGraph("resources/graph.txt");
		}
		catch (IOException e) {
			System.err.println(e);
			System.exit(0);
		}
		
//read in input positions, ie the positions of the nodes as x, y co-ords
		File file = new File("resources/pos.txt");	
		Scanner in = null;
        try 
        {
			in = new Scanner(file);
		} 
        catch (FileNotFoundException e) 
        {
			System.out.println(e);
		}
        
//get the number of nodes
        String topLine = in.nextLine();
        int numberOfNodes = Integer.parseInt(topLine);
        
//produces the coordinateMap of the coordinates, key is the node number, value is a list<int> with the x, y co-ords in  
        for(int i = 0; i < numberOfNodes; i++)
        {
        	String line = in.nextLine();
       
        	String[] parts = line.split(" ");
        	List<Integer> pos = new ArrayList<Integer>();
        	pos.add(Integer.parseInt(parts[1]));
        	pos.add(Integer.parseInt(parts[2]));
        	
        	String key = parts[0];
        	coordinateMap.put(key, pos);
        }
    }

	@Override
	public void notify(Move move) {
		// TODO Auto-generated method stub
		
	}


}
