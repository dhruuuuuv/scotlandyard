package solution.graphics;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import solution.*;
import scotlandyard.*;

//this class is for the actual location of the pin on the map, managed in the "Map" class
public class PinDisplay extends JLabel {
	Graphics2D g;
	private Colour colour;
	List<Integer> coord;
	private String filepath;
	
	PinDisplay(Colour colour, Graphics2D g, String filepath) {
		this.colour = colour;
		this.g = g;
		this.filepath = filepath;
	}

//paint component inherited, not called by us
	public void paintComponent(Graphics g0) {
		super.paintComponent(g0);
//		g = (Graphics2D) g;
		
		BufferedImage pin = null;
		try {
			pin = ImageIO.read(new File("src/solution/graphics/pin" + filepath + ".jpg"));
		}
		catch (IOException e) {
			System.err.println("file input error");
		}
		
		g.drawImage(pin,coord.get(0),coord.get(1),null);
	}

//returns the colour of the pin
	Colour getColour() {
		return colour;
	}

//updates the coordinates of the players	
	void updateLocation(List<Integer> coord) {
		this.coord = coord;
	}

}
