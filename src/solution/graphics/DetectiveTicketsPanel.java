package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.Format.Field;
import java.util.*;
import java.util.concurrent.*;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.*;

public class DetectiveTicketsPanel extends JPanel{
	HashMap<Ticket, Integer> ticketMap;
	Colour colour;
	
    JLabel tField = new JLabel();
    JLabel bField = new JLabel();
    JLabel uField = new JLabel();
    
	JPanel taxiPanel = new JPanel();
	JPanel busPanel = new JPanel();
	JPanel undergroundPanel = new JPanel();
	
	DetectiveTicketsPanel(Colour colour, HashMap<Ticket, Integer> ticketMap) {
		this.ticketMap = ticketMap;
		this.colour = colour;

		GridLayout grid = new GridLayout(0, 1, 3, 3);
		this.setLayout(grid);
		
		Color color;
		
		try {
		    java.lang.reflect.Field field = Class.forName("java.awt.Color").getField(colour.toString());
		    color = (Color)field.get(null);
		} catch (Exception e) {
		    color = null;
		}
		
		this.setBorder(BorderFactory.createLineBorder(color));

		updateTickets(ticketMap);
	}
	
	void display() { 
		this.add(makeTicketPanel());
		this.add(makePlayerPanel());
	}

	JPanel makeTicketPanel() {
				
		JPanel tickets = new JPanel();
		GridLayout grid = new GridLayout();

		tickets.setLayout(grid);
		
        JLabel t = new JLabel("Taxi Tickets: ");
        JLabel u = new JLabel("Underground Tickets: ");
        JLabel b = new JLabel("Bus Ticket: ");
        
        taxiPanel.add(ticketBox(t, tField));
        busPanel.add(ticketBox(b, bField));
        undergroundPanel.add(ticketBox(u, uField));
        
        tickets.add(taxiPanel);
        tickets.add(busPanel);
        tickets.add(undergroundPanel);
        
        return tickets;
	}
	
	JPanel makePlayerPanel() {
		
		JPanel playerPanel =  new JPanel();
		JLabel player = new JLabel();
		
		String str = colour.toString();
		
		player.setText(str + "'s Tickets");
		
		playerPanel.add(player);
		
		return playerPanel;
	}

//makes a box to be added to the field
	Box ticketBox(JLabel info, JLabel field) {
		Border border = BorderFactory.createEmptyBorder(3, 3, 3, 3);
		info.setBorder(border);
		field.setBorder(border);
		Box box = Box.createVerticalBox();
		box.add(info);
		box.add(field);
		return box;
	}
	
	void updateTickets(HashMap<Ticket, Integer> ticketMap) {
		tField.setText(Integer.toString(ticketMap.get(Ticket.Taxi)));
		uField.setText(Integer.toString(ticketMap.get(Ticket.Underground)));
		bField.setText(Integer.toString(ticketMap.get(Ticket.Bus)));
	}
}
