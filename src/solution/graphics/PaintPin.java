package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PaintPin extends JLabel {
	Graphics2D g;
	Colour colour;
	List<Integer> coords;

//object to create and manage pins on the map
	PaintPin(Graphics2D g, Colour colour) {
		this.g = g;
		this.colour = colour;
	}

//sets the coordinate for initialisation and 
	void setCoords(List<Integer> coords) {
		this.coords = coords;
		setColour();
		prodPin();
	}

//updates and repaints, sets coorsd
	void update(List<Integer> coords) {
		this.coords = coords;
		setColour();
		repaint();
	}

//produces the pin object
	void prodPin() {
		if (coords != null) {
			g.fillOval(coords.get(0)-11, coords.get(1)-11, 20, 20);
		}
		
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
	}
	
	void paintComponent() {
		prodPin();
	}
	
	void setColour() {
		switch (colour) {
		case Black: g.setColor(Color.BLACK);
		break;
		case Blue: g.setColor(Color.BLUE);
		break;
		case Green: g.setColor(Color.GREEN);
		break;
		case Yellow: g.setColor(Color.YELLOW);
		break;
		case White: g.setColor(Color.WHITE);
		break;
		case Red: g.setColor(Color.RED);
		break;
		}
	}

}
