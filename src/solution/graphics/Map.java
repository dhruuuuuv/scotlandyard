package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Map extends JLabel {
	BufferedImage img = null;
	Graphics2D g;
	
//produces a JLabel to hold the Map Background	
	Map() {
		readInImage();
		makeGraphics();
		makeJLabel();
	}

//reads in the image from a file
	void readInImage() {
		try
		{
			img = ImageIO.read(new File("resources/map.jpg"));
		}
		catch( IOException e )
		{
			System.out.println(e);
		}
	}

//extracts the 2d graphics from the image and sets the field
	void makeGraphics() {
		
        Graphics2D g2d = img.createGraphics();
		this.g = g2d;
	}
	
	Graphics2D getImageGraphics() {
		return g;
	}
	
	void makeJLabel() {
		ImageIcon icon = new ImageIcon();
		icon = new ImageIcon(img);
		this.setIcon(icon);
	}
}
