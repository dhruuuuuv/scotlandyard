package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.*;

public class GameStatsManager extends JPanel{
	List<JPanel> statPanels = new ArrayList<JPanel>();
//	HashMap<Colour, JPanel> gameStatsMap;
	
    JLabel gsField = new JLabel();
    JLabel wtField = new JLabel();
    JLabel wrField = new JLabel();
    
	JPanel gameStatus;
	JPanel whoseTurn;
	JPanel whatRound;

	
	GameStatsManager(HashMap<String, String> gameStatsMap) {
		GridLayout grid = new GridLayout(0, 1, 3, 3);
		this.setLayout(grid);
		
		gameStatus = new JPanel();
		whoseTurn = new JPanel();
		whatRound = new JPanel();

		
		statPanels.add(gameStatus);
		statPanels.add(whoseTurn);
		statPanels.add(whatRound);

		
		updateStats(gameStatsMap);
	}

//ensure only called after update Stats
	void display() {
        JLabel gs = new JLabel("Game Status: ");
        JLabel wt = new JLabel("Current Player: ");
        JLabel wr = new JLabel("Round Number: ");
        
        gameStatus.add(infoBox(gs, gsField));
        whoseTurn.add(infoBox(wt, wtField));
        whatRound.add(infoBox(wr, wrField));
        
        this.add(gameStatus);
        this.add(whoseTurn);
        this.add(whatRound);
	}

//makes a box to be added to the field
	Box infoBox(JLabel info, JLabel field) {
		Border border = BorderFactory.createEmptyBorder(3, 3, 3, 3);
		info.setBorder(border);
		field.setBorder(border);
		Box box = Box.createVerticalBox();
		box.add(info);
		box.add(field);
		return box;
	}
	
	void updateStats(HashMap<String, String> statStrings) {
		gsField.setText(statStrings.get("gs"));
		wtField.setText(statStrings.get("wt"));
		wrField.setText(statStrings.get("wr"));
	}
	
}
