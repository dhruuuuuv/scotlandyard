package solution.graphics;

import scotlandyard.*;
import solution.*;
import solution.graphics.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Display extends JPanel {
	Graphics2D g;
	
	HashMap<Colour, PaintPin> pinMap = new HashMap<Colour, PaintPin>();
	HashMap<Colour, List<Integer>> currentCoordMap = new HashMap<Colour, List<Integer>>();
	HashMap<String, String> gameStatsMap;
	HashMap<Colour, HashMap<Ticket, Integer>> ctm = new HashMap<Colour, HashMap<Ticket, Integer>>();
	List<Colour> players;
	
	Map map;
	JPanel pins;
	
	JPanel sidePane = new JPanel();
	
	GameStatsManager manager;
	
	JPanel allDetectivePanels = new JPanel();
	
	
//display constructor
	Display(List<Colour> players, HashMap<Colour, List<Integer>> currentCoordMap, HashMap<String, String> gameStatsMap, HashMap<Colour, HashMap<Ticket, Integer>> ctm) {
		this.players = players;
		this.currentCoordMap = currentCoordMap;
		this.gameStatsMap = gameStatsMap;
		this.ctm = ctm;
		sidePane.setLayout(new GridLayout());
		GridLayout grid = new GridLayout();
		allDetectivePanels.setLayout(grid);
		this.setLayout(new BorderLayout());
	}

//updates the coordinate Map of the Players
	void update(HashMap<Colour, List<Integer>> currentCoordMap, HashMap<String, String> gameStatsMap, HashMap<Colour, HashMap<Ticket, Integer>> ctm) {
		this.currentCoordMap = currentCoordMap;
		this.ctm = ctm;
		this.gameStatsMap = gameStatsMap;
		
		updatePins();
		manager.updateStats(gameStatsMap);
		DetectiveTicketPanels(ctm);
		
		repaint();
	}

//initialises the game with the Mpa and the pins, adds them to the Display Frame
	void start() {
		map =  new Map();
		
		this.g = map.getImageGraphics();
		pins = makePins();
		
		manager = new GameStatsManager(gameStatsMap);
		manager.display();
		
		DetectiveTicketPanels(ctm);
		
		
		sidePane.add(manager);
//		sidePane.add(allDetectivePanels);
		
		this.add(map, BorderLayout.LINE_START);
		this.add(pins, BorderLayout.CENTER);
		
		this.add(sidePane, BorderLayout.LINE_END);
		this.add(allDetectivePanels, BorderLayout.PAGE_END);
		
	}

//update the pin objects
	public void updatePins() {
		for (Colour colour : pinMap.keySet()) {
			PaintPin pin = pinMap.get(colour);
			List<Integer> coords = currentCoordMap.get(colour);
			pin.update(coords);
		}
	}
	
	public void DetectiveTicketPanels(HashMap<Colour, HashMap<Ticket, Integer>> ctm) {
		for (Colour colour : ctm.keySet()) {
			DetectiveTicketsPanel dtp = new DetectiveTicketsPanel(colour, ctm.get(colour));
			dtp.display();
			allDetectivePanels.add(dtp);
		}
	}

//for initialising the pins
	public void startPinPositions() {
		for (Colour colour : pinMap.keySet()) {
			PaintPin pin = pinMap.get(colour);
			List<Integer> coords = currentCoordMap.get(colour);
			pin.setCoords(coords);
		}
	}

//overwrites the paintComponent method
	public void paintComponent(Graphics g0) {
		super.paintComponent(g0);
		Graphics2D g = (Graphics2D) g0;
		
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
	}

//makes the pins and returns a JPanel with them all in, also sets the start positions of pins
	JPanel makePins() {
		pins = new JPanel();
		
		for (Colour colour : players) {
			PaintPin pin = new PaintPin(g, colour);
			pinMap.put(colour, pin);
			pins.add(pin);
		}
		
		startPinPositions();	
		return pins;
	}
	

}
